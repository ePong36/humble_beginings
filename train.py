import argparse
import torch
import numpy as np
import torch.optim as optim
import torch.nn as nn
from pytorch_pretrained_bert import BertTokenizer
from dataset_loading import return_dataloader
from utils import init_experiment, log_epoch
from models import SimpleModel
from sklearn.metrics import f1_score
import time


def calculate_metrics(predictions, labels, metrics_list):

    predictions = predictions.argmax(dim=1)

    metrics = dict()
    if "f1" in metrics_list:
        metrics['f1'] = 0 # Too few sentences to predict from
        # metrics['f1'] = f1_score(y_true=labels.cpu(), y_pred=predictions.cpu().numpy(), average='weighted')
    else:
        raise NotImplementedError

    return metrics


def calculate_mean_of_metrics(batch_metrics):
    metrics_sums = {k: 0.0 for k in batch_metrics[0].keys()}
    for batch in batch_metrics:
        for metric, metric_value in batch.items():
            metrics_sums[metric] += metric_value

    mean_metrics = {k: v/len(batch_metrics) for k, v in metrics_sums.items()}
    return mean_metrics


def initialize_model(config):
    model = dict()
    if config['model'] == 'simple':
        args = {'num_classes': 10}
        model['network'] = SimpleModel(args)
    elif config['model'] == 'legalBERT':
        raise NotImplementedError
    else:
        raise Exception("model name '{}' is not recognised".format(config['model']))

    model['network'].to(config['device'])
    model['optimizer'] = optim.Adam(model['network'].parameters(), lr=config['lr'])
    model['criterion'] = torch.nn.CrossEntropyLoss()

    return model


def perfom_epoch(dataloader, model, config, mode):
    batch_losses = []
    batch_metrics = []
    for batch_id, batch in enumerate(dataloader):
        if mode == 'train':
            model['optimizer'].zero_grad()

        data = batch['data'].to(config['device'])
        labels = batch['label'].to(config['device'])

        output = model['network'](data)
        loss = model['criterion'](output, labels)

        if mode =='train':
            loss.backward()
            model['optimizer'].step()

        batch_losses.append(loss.cpu().detach().numpy())

        # Calculate metrics
        metrics = calculate_metrics(output.detach(), labels, config['metrics'])
        batch_metrics.append(metrics)

    average_metrics = calculate_mean_of_metrics(batch_metrics)
    return np.mean(batch_losses), average_metrics


def train(config):
    # Set random seed + Initialise device
    init_experiment(config)

    # Input: Loading tokeniser and dataloaders
    config['tokeniser'] = BertTokenizer.from_pretrained('bert-base-uncased')
    train_dataloader = return_dataloader(config, mode='train')
    test_dataloader = return_dataloader(config, mode='test')

    # Initialise model
    model = initialize_model(config)

    # Iterate over epochs
    epoch_info = dict()
    for epoch in range(1, config['epochs']+1):
        epoch_info['epoch'] = epoch
        model['network'].train()
        epoch_info['train_loss'], epoch_info['train_metrics'] = perfom_epoch(dataloader=train_dataloader,
                                                                             model=model, config=config, mode='train')
        with torch.no_grad():
            model['network'].eval()
            epoch_info['test_loss'], epoch_info['test_metrics'] = perfom_epoch(dataloader=test_dataloader,
                                                                           model=model, config=config, mode='test')

        log_epoch(config, epoch_info)

    if not config['cpu']:
        print("Maximum memory cached for the whole duration of the experiment: {}".format(torch.cuda.max_memory_cached()))


def main():
    parser = argparse.ArgumentParser()
    # Global
    parser.add_argument("--model", type=str, default="simple", choices=['simple', 'legal_BERT'])
    parser.add_argument("--train_data_dir", type=str, default="training/")
    parser.add_argument("--test_data_dir", type=str, default="test/")
    parser.add_argument("--batch_size", type=int, default=32)
    parser.add_argument("--lr", type=float, default=1e-3)
    parser.add_argument("--epochs", type=int, default=10)
    parser.add_argument("--max_num_tokens", type=int, default=512)
    parser.add_argument("--seed", type=int, default=42)
    parser.add_argument("--resume", type=str, help="e.g.: 'results/exp_1/best_model'")
    parser.add_argument("--log_filename", type=str, default='output.log')
    parser.add_argument("--results_dir", type=str, default='results')
    parser.add_argument("--save_model_to_file", type=str, default=None, 
                        help='which file to save nn model to after training')
    parser.add_argument("--tb_log_file", type=str, default='tb_logs',
                        help='where to store tensorboard log')
    parser.add_argument("--metrics", type=str, default='f1')

    # Dataloading specific
    parser.add_argument("--train_load_from_file", type=str)
    parser.add_argument("--train_save_to_file", type=str, help='e.g.: train_data')
    parser.add_argument("--test_load_from_file", type=str)
    parser.add_argument("--test_save_to_file", type=str, help='e.g.: test_data')
    parser.add_argument("--labels_file_path", type=str)

    # For debugging
    parser.add_argument("-d", dest="debug", action="store_true")
    parser.add_argument("-cpu", dest="cpu", action="store_true")
    parser.add_argument("-t", dest="temp", action="store_true")
    parser.add_argument("-dummy_data", dest="dummy_data", action="store_true")
    parser.add_argument("--verbosity", type=int, default=1)

    args = parser.parse_args()
    args.metrics = args.metrics.lower().replace(" ", "").split(',')

    t0 = time.time()
    train(vars(args))
    print("\n-------------- Done in {0:.4f} [hours]---------------".format((time.time() - t0) / (60 * 60)))


if __name__ == '__main__':
    main()
