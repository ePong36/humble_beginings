import torch
import torch.nn as nn
from pytorch_pretrained_bert import BertModel
import numpy as np


class SimpleModel(torch.nn.Module):
    def __init__(self, args):
        super(SimpleModel, self).__init__()

        self.BERT = BertModel.from_pretrained('bert-base-uncased')
        # 768 is the number of dimensions the bert model gives as output.
        self.top_classificiation = nn.Linear(768, args['num_classes'])
        self.softmax = nn.Softmax()

    def forward(self, x):
        """
        :param x: [batch_size(number of sentences), #tokens, token_id]
        :return: probability per token per class
        """
        encoded_layers, sentence_embedding = self.BERT(x)
        x = self.top_classificiation(sentence_embedding)
        x = self.softmax(x)
        return x

    def do_gradient_update_BERT_weights(self, do_gradient=True):
        for name, param in self.BERT.named_parameters():
            param.requires_grad = do_gradient
        print("{} BERT parameters".format("Freezed" if not do_gradient else "Un-freezed"))
