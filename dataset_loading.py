
import numpy as np
import torch
import more_itertools as mit
import pickle
from torch.utils.data import DataLoader, Dataset


class legal_dataset(Dataset):
    def __init__(self, config, args):
        if args['load_from_file'] is None:
            print("Creating data from directory: '{}'".format(args['data_dir']))
            self.data = self.load_data(config, args)
            
        else:
            print("Loading pre-saved data from {}".format(args['load_from_file']))
            self.data = pickle.load(open(args['load_from_file'], "rb"))
        
        if not args['save_to_file'] is None:
            print("Saving data to '{}'".format(args['save_to_file']))
            pickle.dump(self.data, open(args['save_to_file'], 'wb'))

    def __len__(self):
        return len(self.data['data'])

    def __getitem__(self, index):
        return {'data': torch.tensor(self.data['data'][index], requires_grad=False).long(),
                'label': torch.tensor(self.data['labels'][index], requires_grad=False).long()}

    @staticmethod
    def pad_tokens(list_of_tokens, max_length):
        return list(mit.padded(list_of_tokens, '[PAD]', max_length))
    
    def load_data(self, config, args, add_CLS_SEP=True):

        raise NotImplementedError
        return {'data': data_list, 'labels': labels_list}


class DummyDataset(legal_dataset):
    def __init__(self, config, args):
        self.data = dict()
        number_special_tokens_in_sentence = 2

        num_classes = 10

        sentences = ["But soft, what light through yonder window breaks?",
                     "It is the east and Juliet is the sun!",
                     "Arise, fair sun, and kill the envious moon, Who is already sick and pale with grief",
                     "That thou her maid art far more fair than she. ",
                     "Be not her maid, since she is envious; Her vestal livery is but sick ",
                     "and green, And none but fools do wear it. Cast it off. It is my lady, O, it is my love!",
                     "O that she knew she were! She speaks, yet she says nothing; what of that? Her eye discourses, "
                     "I will answer it.",
                     "I am too bold: 'tis not to me she speaks.",
                     "Two of the fairest stars in all the heaven, Having some business, do entreat her eyes To twinkle "
                     "in their spheres till they return.",
                     "What if her eyes were there, they in her head?",
                     "The brightness of her cheek would shame those stars, As daylight doth a lamp.",
                     "Her eyes in heaven Would through the airy region stream so bright That birds would sing and "
                     "think it were not night.",
                     "See how she leans her cheek upon her hand O that I were a glove upon that hand, "
                     "That I might touch that cheek!"]
        labels = [np.random.randint(num_classes) for sentence in sentences]

        if config['verbosity'] > 1:
            print("sentences: {}".format(sentences))
            print("labels: {}".format(labels))

        max_token_sentence = config['max_num_tokens'] - number_special_tokens_in_sentence
        actual_max_len = -1

        tokenized_sentences = []
        tokenized_sentences_labels = []
        for id, sentence in enumerate(sentences):
            tokenized_sentence = config['tokeniser'].tokenize(sentence)

            if len(tokenized_sentence) > actual_max_len:
                actual_max_len = len(tokenized_sentence)

            if len(tokenized_sentence) > max_token_sentence:
                # Split into max number of token sentences (consider changing padding for last sentence with less than
                #     maximum number of words.
                for word_interval in range(0, len(tokenized_sentence), max_token_sentence):
                    end_point = min(word_interval+max_token_sentence)
                    s = tokenized_sentence[word_interval:end_point]
                    if len(s) < max_token_sentence:
                        s = tokenized_sentence[max(0, end_point-max_token_sentence):end_point]
                        assert len(s) == max_token_sentence
                    tokenized_sentences.append(['[CLS]'] + list(s) + ['[SEP]'])
                    tokenized_sentences_labels.append(labels[id])
            else:
                tokenized_sentences.append(['[CLS]'] + list(tokenized_sentence) + ['[SEP]'])
                tokenized_sentences_labels.append(labels[id])

        if config['verbosity'] > 1:
            print("tokenized_sentences: {}".format(tokenized_sentences))
            print("tokenized_sentences_labels: {}".format(tokenized_sentences_labels))

        data_ids = []
        for sentence in tokenized_sentences:
            data_ids.append(config['tokeniser'].convert_tokens_to_ids(
                self.pad_tokens(sentence, max_length=min(actual_max_len + number_special_tokens_in_sentence,
                                                         config['max_num_tokens']))))

        if config['verbosity'] > 1:
            print("data_ids: {}".format(data_ids))
            print("tokenized_sentences_labels: {}".format(tokenized_sentences_labels))

        self.data['data'] = data_ids
        self.data['labels'] = tokenized_sentences_labels


# def collate_funct(batch):
#     return batch


def return_dataloader(config, mode):
    allowed_modes = ['train', 'test']
    if not mode in allowed_modes:
        assert Exception("mode '{}' not reckognized. Allowed modes: {}".format(mode, allowed_modes))

    args = {'data_dir': config[mode+'_data_dir'], 'load_from_file': config[mode+'_load_from_file'],
            'save_to_file': config[mode+'_save_to_file']}

    if config['dummy_data']:
        data_set = DummyDataset
    else:
        data_set = legal_dataset
    return DataLoader(data_set(config, args), batch_size=config['batch_size'])#, collate_fn=collate_funct)

