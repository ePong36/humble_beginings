import random
import numpy as np
from torch.backends import cudnn
from torch.distributions.categorical import *
import os
import datetime
import logging
import sys
import re


def get_time():
    return datetime.datetime.now().strftime('%d-%m_%H:%M')


def init_experiment(config):
    exp_info = dict()

    # Adjust for debugging
    if config['debug']:
        exp_info['batch_size'] = 2
        exp_info['epochs'] = 1

    # Random seeds and devices
    set_seed(config['seed'])
    if torch.cuda.is_available() and not config['cpu']:
        exp_info['cpu'] = False
        exp_info['device'] = torch.device('cuda')
        if config['verbosity'] >= 2:
            print(torch.__version__)
            print(torch.version.cuda)
            print(cudnn.version())
    else:
        exp_info['cpu'] = True
        exp_info['device'] = torch.device('cpu')

    # Experiment naming
    if config['resume'] is None:
        exp_info['starting_epoch'] = 1
        exp_info['train_losses'] = []
        exp_info['eval_losses'] = []
        exp_info['checkpoint'] = None
        exp_info['exp_dir'] = os.path.join(config['results_dir'], config['model'])
        if config['temp']:
            exp_name = "Exp_temp"
        else:
            if not os.path.isdir(exp_info['exp_dir']) or len(os.listdir(exp_info['exp_dir'])) == 0 \
                    or os.listdir(exp_info['exp_dir']) == ['Exp_temp']:
                exp_num = 1
            else:
                latest_exp_name = sorted([a for a in os.listdir(exp_info['exp_dir']) if not a == 'Exp_temp'],
                                         key=natural_keys)[-1]
                exp_num = int(latest_exp_name.split('_')[1]) + 1
            exp_name = "Exp_{}_{}".format(exp_num, datetime.date.today())

        exp_info['exp_dir'] = os.path.join(exp_info['exp_dir'], exp_name)
        os.makedirs(exp_info['exp_dir'], exist_ok=True)
        exp_info['log'] = return_logger(os.path.join(exp_info['exp_dir'], config['log_filename']))
        exp_info['log'].info('Initialised: {}'.format(exp_info['exp_dir']))
    else:
        raise NotImplementedError

    config.update(exp_info)
    print_exp_info(config)


def set_seed(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
        cudnn.enabled = True
        cudnn.benchmark = True
        cudnn.deterministic = True


def natural_keys(text):
    """
    Returns the key list for float numbers in file names for more proper sorting
    """

    def atof(text):
        """
        Used for sorting numbers, in float format
        """
        try:
            retval = float(text)
        except ValueError:
            retval = text
        return retval

    return [atof(c) for c in re.split(r'[+-]?([0-9]+(?:[.][0-9]*)?|[.][0-9]+)', text)]


def return_logger(log_file):
    """
    Creates the logger
    """
    log = logging.getLogger(__name__)
    log.setLevel(logging.DEBUG)
    fh = logging.FileHandler(log_file)
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.INFO)
    formatter = logging.Formatter(fmt='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    log.addHandler(fh)
    log.addHandler(ch)

    return log


def print_exp_info(config):
    str_log =  \
        "\n================================\n"\
        "Running {} from epoch {}:\n"\
        "   Max num of epochs: {}\n"\
        "   Learning rate: {}\n"\
        "   Device: {}\n"\
        "   Seed: {}\n"\
        "   Debugging mode: {}\n"\
        "================================".format(
            config['model'], config['starting_epoch'],
            config['epochs'],
            config['lr'],
            config['device'],
            config['seed'],
            config['debug'],
           )

    config['log'].info(str_log)


def log_epoch(config, epoch_info):
    str_log = "[{}/{}] Train loss: {}, Test loss: {}  ||  Train metrics: {}, Test metrics: {}".format(
        epoch_info['epoch'],
        config['epochs'] + 1,
        epoch_info['train_loss'],
        epoch_info['test_loss'],
        epoch_info['train_metrics'],
        epoch_info['test_metrics']
    )
    config['log'].info(str_log)
